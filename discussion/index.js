console.log("TGIF!");

// Array Methods

// [SECTION] Mutator Methods

/*
    - Mutator methods are functions that "mutate" or change an array after they're created
    - These methods manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon fruit'];

console.log('Current Array:');
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated Array from push Method: ');
console.log(fruits);

/*
        - Adds an element in the end of an array AND returns the array's length
        - Syntax
            arrayName.push();
*/

fruits.push('Avocado', 'Guava');
console.log('Mutated Array from push Method:');
console.log(fruits);

// pop()
/*
        - Removes the last element in an array AND returns the removed element
        - Syntax
            arrayName.pop();
    */
let removeFruit = fruits.pop();
console.log(removeFruit);
console.log("Mutated Array from pop method: ");
console.log(fruits);

// unshift()
/*
        - Adds one or more elements at the beginning of an array
        - Syntax
            arrayName.unshift('elementA');
            arrayName.unshift('elementA', elementB);
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated Array from unshift method:');
console.log(fruits);


// shift()
/*
    - Removes an element at the beginning of an array AND returns the removed element
    - Syntax
        arrayName.shift();
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated Array from shift method:');
console.log(fruits);

// splice()
/* 
    - Simultaneously removes elements from a specified index number and adds elements
    - Syntax
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1,2,'Lime', 'Cherry');
console.log('Mutated Array from splice method:');
console.log(fruits);

// sort()
/*
        - Rearranges the array elements in alphanumeric order
        - Syntax
            arrayName.sort();
    */
fruits.sort();
console.log('Mutated Array from sort method:');
console.log(fruits);

// reverse()
/*
        - Reverses the order of array elements
        - Syntax
            arrayName.reverse();
    */

fruits.reverse();
console.log('Mutated Array from reverse method:');
console.log(fruits);

// [SECTION] Non-Mutator Methods

/*
        - Non-Mutator methods are functions that do not modify or change an array after they're created
        - These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
    */

let countries = ['PH', 'US', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf()
/*
    - Returns the index number of the first matching element found in an array
    - If no match was found, the result will be -1.
    - The search process will be done from first element proceeding to the last element
    - Syntax
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidIndex = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidIndex);

let secondIndex = countries.indexOf('PH', 1);
console.log('Result of indexOf method: ' + secondIndex);

// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array
    - The search process will be done from last element proceeding to the first element
    - Syntax
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOf(searchValue, fromIndex);
*/
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

// slice()

/*
    - Portions/slices elements from an array AND returns a new array
    - Syntax
        arrayName.slice(startingIndex);
        arrayName.slice(startingIndex, endingIndex);
*/

let slicedArrayA = countries.slice(2);
console.log('Result from slice method: ' + slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method: ' + slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method: ' + slicedArrayC);

// toString()
/*
    - Returns an array as a string separated by commas
    - Syntax
        arrayName.toString();
*/
let stringArray = countries.toString();
console.log('Result from toString method: ' + stringArray);

// concat()


let tasksArrayA = ['drink HMTL', 'eat JavaScript'];
let tasksArrayB = ['inhale CSS', 'breath SASS'];
let tasksArrayC = ['get GIT', 'be Node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method: ');
console.log(tasks);

// combine multiple arrays
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result from concat method: ');
console.log(allTasks);

// combine arrays with elements
let combinedTasks = tasksArrayA.concat("smell Expresss", 'throw React');
console.log('Result from concat method: ');
console.log(combinedTasks);

// join()
/*
    - Returns an array as a string separated by specified separator string
    - Syntax
        arrayName.join('separatorString');
*/

console.log(allTasks.join());
console.log(allTasks.join(''));

// [SECTION] Iteration Methods
/*
    - Iteration methods are loops designed to perform repetitive tasks on arrays
    - Iteration methods loops over all items in an array.
    - Useful for manipulating array data resulting in complex tasks
    - Array iteration methods normally work with a function supplied as an argument
    - How these function works is by performing tasks that are pre-defined within an array's method.
*/


// forEach()
/*
    - Similar to a for loop that iterates on each array element.
    - For each item in the array, the anonymous function passed in the forEach() method will be run.
    - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
    - Variable names for arrays are normally written in the plural form of the data stored in an array
    - It's common practice to use the singular form of the array content for parameter names used in array lo
- It's common practice to use the singular form of the array content for parameter names used in array loops
    - forEach() does not return anything.
    - Syntax
        arrayName.forEach(function(indivElement) {
            statement
        })
*/

allTasks.forEach(function(task){

	console.log(task);

});

let filteredTasks = [];

allTasks.forEach(function(task){

	if(task.length > 10){
		filteredTasks.push(task);
	}

});

console.log('Resultof filteredTasks:');
console.log(filteredTasks);

// map()
/* 
    - Iterates on each element AND returns new array with different values depending on the result of the function's operation
    - This is useful for performing tasks where mutating/changing the elements are required
    - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
    - Syntax
        let/const resultArray = arrayName.map(function(indivElement))
*/

let taskMap = allTasks.map(function(task){

	if(task.length > 10){
		task = "Hello World";
	}

	return task;
})

console.log(allTasks);
console.log(taskMap);

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
    return number * number;
});

console.log("Original Array:");
console.log(numbers);
console.log("Result of map method:");
console.log(numberMap);

// every()
/*
        - Checks if all elements in an array meet the given condition
        - This is useful for validating data stored in arrays especially when dealing with large amounts of data
        - Returns a true value if all elements meet the condition and false if otherwise
        - Syntax
            let/const resultArray = arrayName.every(function(indivElement) {
                return expression/condition;
            })
    */

let allValid = numbers.every(function(number) {
        return (number < 3);
    });
    console.log("Result of every method:");
    console.log(allValid);

/*
    - Checks if at least one element in the array meets the given condition
    - Returns a true value if at least one element meets the condition and false if otherwise
    - Syntax
        let/const resultArray = arrayName.some(function(indivElement) {
            return expression/condition;
        })
*/
let someValid = numbers.some(function(number) {
        return (number < 2);
    });
    console.log("Result of some method:");
    console.log(someValid);

    // Combining the returned result from the every/some method may be used in other statements to perform consecutive results
    if (someValid) {
        console.log('Some numbers in the array are greater than 2');
    };

    /*
        - Returns new array that contains elements which meets the given condition
        - Returns an empty array if no elements were found
        - Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
        - Mastery of loops can help us work effectively by reducing the amount of code we use
        - Several array iteration methods may be used to perform the same result
- Syntax
        let/const resultArray = arrayName.filter(function(indivElement) {
            return expression/condition;
        })
*/

let filterValid = numbers.filter(function(num) {
    	
    	return (num < 3);

});

console.log('Result of filter method:');
console.log(filterValid);

// no elements found
let nothingFound = numbers.filter(function(num) {

	return (num === 0);

});

console.log('Result of filter method:');
console.log(nothingFound);

// includes()
/*
    - includes() method checks if the argument passed can be found in the array.
    - it returns a boolean which can be saved in a variable.
        - returns true if the argument is found in the array.
        - returns false if it is not.
    - Syntax:
        arrayName.includes(<argumentToFind>)
*/

let number1 = numbers.includes(4);

console.log(number1);

let number2 = numbers.includes(8);

console.log(number2);

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(pro) {

	return pro.toLowerCase().includes('m');

})

console.log(filteredProducts);

// reduce()
/* 
    - Evaluates elements from left to right and returns/reduces the array into a single value
    - Syntax
        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
            return expression/operation
        })
    - The "accumulator" parameter in the function stores the result for every iteration of the loop
    - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
    - How the "reduce" method works
1. The first/result element in the array is stored in the "accumulator" parameter
        2. The second/next element in the array is stored in the "currentValue" parameter
        3. An operation is performed on the two elements
        4. The loop repeats step 1-3 until all elements have been worked on
*/

let interation = 0;

let reducedArray = numbers.reduce(function(x, y) {

	console.warn('current iteration: ' + ++interation);
	console.log('accumulator: ' + x);
	console.log('CurrentValue: ' + y);

	return x + y;
});

console.log('Result of reduce method:' + reducedArray);

// [1, 2, 3, 4, 5];

// 1 + 2 = 3 + 3 = 6 + 4 = 10 + 5 = 15

let list = ["Thank", "G", "Its", "Friday"];

let reducedList = list.reduce(function(x, y) {

	return x + " " + y;

});

console.log('Result of reduce method:' + reducedList);

// ["Thank", "G", "Its", "Friday"];

// Thank + " " + G = Thank G + " " + Its = Thank G Its + " " + Friday = Thank G Its Friday

